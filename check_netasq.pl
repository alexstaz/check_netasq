#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;
use File::Temp qw/ tempfile /;
use Data::Dumper;

our %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3);

our $dbg=0;
our $host='';
our $username;
our $password;
my $check='ha';
our $nsrpc_path = '/usr/local/bin/';

GetOptions("debug" => \$dbg,
    "host=s" => \$host,
    "username=s" => \$username,
    "password=s" => \$password,
    "check:s" => \$check,
    "path:s" => \$nsrpc_path);

my ($fh, $filename) = tempfile(UNLINK => 1);

if ($check eq 'ha')
{
    print $fh "HA INFO\n";
    print $fh "HA INFO PEER\n";
    my $ret = exec_command($filename);
    unlink($filename);
    $ret = parse_result($ret);
    #Checking Peer
    if (!exists($ret->{'HA INFO PEER'}))
    {
        print "ERROR : Peer Firewall down\n";
        exit($ERRORS{'CRITICAL'});
    }
    elsif ($ret->{'HA INFO PEER'}->{'Quality'} ne '100' or $ret->{'HA INFO'}->{'Quality'} ne '100')
    {
        print "ERROR : Not good quality, please see interfaces\n";
        exit($ERRORS{'CRITICAL'});
    }
    elsif ($ret->{'HA INFO PEER'}->{'Sync'} ne '1' or $ret->{'HA INFO'}->{'Sync'} ne '1')
    {
        print "WARNING : Firewall not synchronised\n";
        exit($ERRORS{'WARNING'});
    }
    elsif ($ret->{'HA INFO'}->{'Licence'} ne 'Master' && $ret->{'HA INFO'}->{'State'} eq 'Active')
    {
        print "WARNING : Master firewall is passive. Normal ?";
        exit($ERRORS{'WARNING'});
    }
    else
    {
        print "Ok\n";
        exit($ERRORS{'OK'});
    }
}
elsif ($check eq 'raid')
{
    print $fh "MONITOR RAID\n";
    my $ret = exec_command($filename);
    unlink($filename);
    $ret = parse_result($ret);

    if ($ret->{'MONITOR RAID'}->{'RAID_ARRAY_0'}->{'Status'} ne 'Optimal')
    {
        print "ERROR : RAID is degraded\n";
        print "Disk1 : ".$ret->{'MONITOR RAID'}->{'DISK_1'}->{'Status'}."\n";
        print "Disk2 : ".$ret->{'MONITOR RAID'}->{'DISK_2'}->{'Status'}."\n";
        exit($ERRORS{'CRITICAL'});
    }
    else
    {
        print "Ok. Status Optimal.\n";
        exit($ERRORS{'OK'});
    }
}

sub exec_command
{
    my ($filename) = @_;
    print "Starting exec_command\n" if $dbg;
    print "Using this tempfile : $filename\n" if $dbg;
    chdir($nsrpc_path);
    my $ret = `./nsrpc -C 60 -R 60 -c $filename $username:$password\@$host`;
    print $ret if $dbg;
    return $ret;
}

sub parse_result
{
    print "Parsing result\n" if $dbg;
    my ($toparse) = @_;
    my $res;
    my $cmd="";
    my $cmd2="";
    my $start=0;
    my $method=0;
    my @cmds = split("\n",$toparse);
    foreach (@cmds)
    {
        if (/^Netasq> (.*)/)
        {
            $cmd=$1;
        }
        if (/^100 code=00a00100/)
        {
            $start=0;
            $method=0;
        }
        if ($start==1)
        {
            if (/^\[(.*)]/)
            {
                $cmd2=$1;
                $method=1;
            }
            if ($method==0)
            {
                my ($key,$val)=split('=',$_);
                $res->{$cmd}->{$key}=$val;
            }
            elsif ($method==1)
            {
                if (/=/)
                {
                    my ($key,$val)=split('=',$_);
                    $res->{$cmd}->{$cmd2}->{$key}=$val;
                }
            }
        }
        if (/^101 code=00a01000/)
        {
            $start=1;
        }
        if (/^Authentication failed/)
        {
            print "Error : Authentication failed\n";
            exit($ERRORS{'UNKNOWN'});
        }
        if (/^200 code=(\d+) msg="(.+)"/ && $#cmds==1)
        {
            print "Error : $2\n";
            exit($ERRORS{'UNKNOWN'});
        }
    }
    print Dumper($res) if ($dbg);
    return $res;
}
