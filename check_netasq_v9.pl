#!/usr/bin/perl

use strict;
use warnings;

use Getopt::Long;
use Data::Dumper;
use MIME::Base64;
use LWP;
use HTTP::Cookies;
use XML::LibXML;

use IO::Socket::SSL;
IO::Socket::SSL::set_client_defaults(SSL_verify_mode => SSL_VERIFY_NONE );
$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'}=0;


our %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3);

our $dbg=0;
our $host='';
our $username;
our $password;
my $check='ha';
my $checkpasv = 0;

GetOptions("debug" => \$dbg,
    "host=s" => \$host,
    "username=s" => \$username,
    "password=s" => \$password,
    "check:s" => \$check,
    "checkpasv" => \$checkpasv);

#Connect
my $browser = LWP::UserAgent->new( );
$browser->cookie_jar( {} );
#$browser->ssl_opts(verify_hostname => 0 );
my $req = HTTP::Request->new(POST => 'https://'.$host.'/auth/admin.html');
$req->content_type('application/x-www-form-urlencoded; charset=UTF-8');
$req->content('uid='.encode_base64($username,'').'&pswd='.encode_base64($password,'').'&app=monitoring');
my $res = $browser->request($req);
my $xml = XML::LibXML->load_xml(string => $res->content)->getDocumentElement();
if ($res->is_success && $xml->getAttribute('msg') eq 'AUTH_SUCCESS' && $xml->getAttribute('value') eq 'ok') {
    #Ok we are logged
    my $cookie = $browser->cookie_jar();
    $browser = LWP::UserAgent->new( );
    $browser->cookie_jar($cookie);
    $browser->ssl_opts(verify_hostname => 0);
    $req = HTTP::Request->new(POST => 'https://'.$host.'/api/auth/login');
    $req->content_type('application/x-www-form-urlencoded; charset=UTF-8');
    $req->content('reqlevel=admin,modify,base,contentfilter,log,log_read,filter,filter_read,globalfilter,vpn,vpn_read,report,report_read,pki,object,globalobject,user,network,route,maintenance,asq,pvm&app=monitoring');
    $res = $browser->request($req);
    $xml = XML::LibXML->load_xml(string => $res->content)->getDocumentElement();
    if ($res->is_success && $xml->getAttribute('msg') eq "OK" && $xml->getAttribute('code') eq "100")
    {
        my $sessionid=$xml->getElementsByTagName('sessionid')->[0]->getFirstChild->getData;
        $browser = LWP::UserAgent->new( );
        $browser->cookie_jar($cookie);
        $browser->ssl_opts(verify_hostname => 0);
        $req = HTTP::Request->new(POST => 'https://'.$host.'/api/commands');
        $req->content_type('application/x-www-form-urlencoded; charset=UTF-8');
        if ($check eq 'ha')
        {
            $req->content('cmd1=HA INFO&sessionid='.$sessionid.'&id=0');
            $res = $browser->request($req);
            $xml = XML::LibXML->load_xml(string => $res->content)->getDocumentElement();
            $browser = LWP::UserAgent->new( );
            $browser->cookie_jar($cookie);
            $browser->ssl_opts(verify_hostname => 0);
            $req = HTTP::Request->new(POST => 'https://'.$host.'/api/auth/logout');
            $req->content_type('application/x-www-form-urlencoded; charset=UTF-8');
            $req->content('sessionid='.$sessionid.'&id=0');
            $browser->request($req);
            if ($res->is_success && $xml->getAttribute('msg') eq "OK" && $xml->getAttribute('code') eq "100")
            {
                print $xml->toString() if $dbg;
                my $sec=0;
                my $fi = $xml->getElementsByTagName('command')->[0]->getElementsByTagName('section')->[0]->getAttribute('title');
                if ($fi eq 'Cluster' or $fi eq 'Notifications')
                {
                    $sec=1;
                } 
                my $firewall1 = parse_result($xml->getElementsByTagName('command')->[0]->getElementsByTagName('section')->[$sec]);
                my $firewall2 = parse_result($xml->getElementsByTagName('command')->[0]->getElementsByTagName('section')->[$sec+1]);
                if (!defined($firewall1) or $firewall1->{'Reply'} ne '1' or $firewall2->{'Reply'} ne '1')
                {
                    print "ERROR : Peer Firewall down\n";
                    exit($ERRORS{'CRITICAL'});
                }
                elsif ($firewall1->{'Quality'} ne '100' or $firewall2->{'Quality'} ne '100')
                {
                    print "ERROR : Not good quality, please see interfaces\n";
                    exit($ERRORS{'CRITICAL'});
                }
                elsif ($firewall1->{'IsConfigSync'} ne '1' or $firewall2->{'IsConfigSync'} ne '1')
                {
                    print "WARNING : Firewall not synchronised\n";
                    exit($ERRORS{'WARNING'});
                }
                elsif ( $checkpasv == 1 && (($firewall1->{'Licence'} eq 'Slave' && $firewall1->{'State'} eq 'Running') or ($firewall2->{'Licence'} eq 'Slave' && $firewall2->{'State'} eq 'Running')))
                {
                    print "WARNING : Master firewall is passive. Normal ?";
                    exit($ERRORS{'WARNING'});
                }
                else
                {
                    print "Ok\n";
                    exit($ERRORS{'OK'});
                }
            }
            else
            {
                print "An error occured when retrieving HA INFO\n";
                exit($ERRORS{'UNKNOWN'});
            }

        }
        elsif ($check eq 'raid')
        {
            $req->content('cmd1=MONITOR RAID&sessionid='.$sessionid.'&id=0');
            $res = $browser->request($req);
            $xml = XML::LibXML->load_xml(string => $res->content)->getDocumentElement();
            $browser = LWP::UserAgent->new( );
            $browser->cookie_jar($cookie);
            $browser->ssl_opts(verify_hostname => 0);
            $req = HTTP::Request->new(POST => 'https://'.$host.'/api/auth/logout');
            $req->content_type('application/x-www-form-urlencoded; charset=UTF-8');
            $req->content('sessionid='.$sessionid.'&id=0');
            $browser->request($req);
            if ($res->is_success && $xml->getAttribute('msg') eq "OK" && $xml->getAttribute('code') eq "100")
            {
                my $firewall = parse_result($xml->getElementsByTagName('command')->[0]->getElementsByTagName('section'));
                if ($firewall->{'RAID_ARRAY_0'}->{'Status'} ne 'Optimal')
                {
                    print "ERROR : RAID is degraded\n";
                    print "Disk1 : ".$firewall->{'DISK_1'}->{'Status'}."\n";
                    print "Disk2 : ".$firewall->{'DISK_2'}->{'Status'}."\n";
                    exit($ERRORS{'CRITICAL'});
                }
                else
                {
                    print "Ok. Status Optimal.\n";
                    exit($ERRORS{'OK'});
                }
            }
            else
            {
                print "An error occured when retrieving MONITOR RAID\n";
                exit($ERRORS{'UNKNOWN'});
            }
        }
    }
    else
    {
        print "An error occured while getting rights\n";
        exit($ERRORS{'UNKNOWN'});
    }
}
else
{
    print "An error occured while login in\n";
    exit($ERRORS{'UNKNOWN'});
}

exit;

sub parse_result
{
    print "Parsing result\n" if $dbg;
    my @toparse = @_;
    print Dumper(@toparse) if $dbg;
    my $res;
    if ($toparse[0])
    {
        if ($#toparse > 1)
        {
            foreach my $sec (@toparse)
            {
                my $sec_title = $sec->getAttribute('title');
                foreach ($sec->getElementsByTagName('key'))
                {
                    $res->{$sec_title}->{$_->getAttribute('name')} = $_->getAttribute('value');
                }
            }        
        }
        else
        {
            $res->{'title'}=$toparse[0]->getAttribute('title');
            foreach ($toparse[0]->getElementsByTagName('key'))
            {
                $res->{$_->getAttribute('name')} = $_->getAttribute('value');
            }
        }
    }
    print Dumper($res) if ($dbg);
    return $res;
}
