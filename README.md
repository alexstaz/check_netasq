# Check Netasq Firewall
===============================


## Introduction

We need to monitor High Avaibility, Raid on Netasq firewall. Theses value was not available with SNMP, so we use linux command line NSRPC available on Netasq website.


## Requirements

-  Nagios or monitoring software
-  Perl with theses modules
    -  File::Temp (for v8)
    -  Getopt::Long
    -  Data::Dumper
    -  MIME::Base64 (for v9)
    -  LWP (for v9)
    -  HTTP::Cookies (for v9)
    -  XML::LibXML (for v9)
-  Linux NSRPC command line tool (available on Netasq website) for v8


## Use

        ./check_netasq.pl --host HOST --username USERNAME --password PASSWORD --check VALUE [--debug]

Options :

    - --debug : to enable debugging
    - --check : type of check, raid or ha (default: ha)
    - --checkpasv : when checking HA, check if master licensed firewall is master (default no) for v9
    - --path : path for nsrpc binarie (default: /usr/local/bin/) for v8
